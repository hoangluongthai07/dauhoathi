﻿using System;

class Program
{
    static void Main()
    {               
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        Console.Write("Nhập số lượng dấu hoa thị cần vẽ: ");
        int soLuong = Convert.ToInt32(Console.ReadLine());
        for (int i = 0; i < soLuong; i++)
        {
            Console.Write("* ");
        }
        System.Console.WriteLine("");
    }
}
